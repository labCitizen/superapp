import { BrowserRouter } from "react-router-dom";
import { FilterContextProvider } from "./store/filter-context";
import { MeetingContextProvider } from "./store/meeting-context";
import { ModalContextProvider } from "./store/modal-context";

import App from "./App";
import ReactDOM from "react-dom";
import reportWebVitals from "./reportWebVitals";
import "./index.css";

// QUESTION: How to use several Context States properly?

ReactDOM.render(
  <ModalContextProvider>
  <MeetingContextProvider>
    <FilterContextProvider>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </FilterContextProvider>
  </MeetingContextProvider>
  </ModalContextProvider>,
  document.getElementById("root")
);

reportWebVitals();
