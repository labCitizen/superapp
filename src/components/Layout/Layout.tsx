import React from 'react'
import MainNavigation from "../Navigation/MainNavigation";

const Layout: React.FC<{ children: React.ReactChild }>= (props) => {
  return (
    <div>
      <MainNavigation />
      <main>{props.children}</main>
    </div>
  );
};

export default Layout;
