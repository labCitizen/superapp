import styled from "styled-components";

export const Select = styled.select`
  color: var(--sw-color-tertiary);
  width: 90%;
  font: inherit;
  font-size: 1.2rem;
  font-weight: bold;
  padding: 1rem;
  border-radius: 3px;
  border: none;
  border: 1px solid var(--sw-color-tertiary);
  outline: none;
  background-color: var(--sw-color-white);
  cursor: pointer;

  & {
    -webkit-appearance: none;
    -moz-appearance: none;
    text-indent: 1px;
    text-overflow: '';
  }

  &:not(:last-child) {
    margin-bottom: 2rem;
  }

  & option {
    font-size: 1.2rem;
  }
`;
