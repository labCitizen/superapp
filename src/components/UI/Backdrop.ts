import styled from "styled-components";

export const Backdrop = styled.div`
  position: absolute;
  top: var(--sw-nav-height-desktop);
  display: none;
  left: 0;
  height: var(--sw-component-height-desktop);
  width: 100%;
  background-color: rgba(0, 0, 0, 0.3);
`;