import styled from "styled-components";

export const Button = styled.button`
  min-width: 10rem;
  color: var(--sw-color-white);
  font: inherit;
  font-size: 1.4rem;
  padding: 1rem;
  border: none;
  border-radius: 3px;
  outline: none;
  cursor: pointer;
  background-color: var(--sw-color-tertiary);
  transition: background-color 0.3s;

  &:nth-child(2) {
    margin-left: 1rem;
  }

  &:active,
  &:hover {
    outline: none;
    background-color: var(--sw-color-tertiary-dark);
  }

`;

export const ButtonOutline = styled(Button)`
  color: var(--sw-color-tertiary);
  border: 1px solid var(--sw-color-tertiary);
  background-color: var(--sw-color-white);

  &:active,
  &:hover {
    color: var(-sw-color-tertiary-dark);
    border: 1px solid var(--sw-color-tertiary-dark);
    background-color: var(--sw-color-white);
  }
`;
