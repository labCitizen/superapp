import classes from './Card.module.css'

const Card: React.FC<React.ReactFragment> = (props) => {
    return (
        <div className={classes.card}>
            {props.children}
        </div>
    )
}

export default Card;