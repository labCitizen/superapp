import "./Modal.css";
import { useContext } from "react";
import { ModalContextObj } from "../../models/ContextObject.class";
import ModalContext from "../../store/modal-context";

// QUESTION: Which is better, css modules or general import?

const Modal: React.FC<React.ReactFragment> = (props) => {

  const ModalCtx = useContext<ModalContextObj>(ModalContext);
  const modalIsOpen = ModalCtx.modalIsOpen();

  return (
    <div style={{ display: modalIsOpen ? "block" : "none" }}>
      <div className="modal-backdrop"></div>
      <div className="modal modal-flex">
        {props.children}
      </div>
    </div>
  );
};

export default Modal;
