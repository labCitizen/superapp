import { useContext } from "react";
import { ModalContextObj } from "../../models/ContextObject.class";
import ModalContext from "../../store/modal-context";
import FormMeeting from '../Meeting/FormMeeting';
import Modal from "../UI/Modal";

const MeetingModal = () => {

  const ModalCtx = useContext<ModalContextObj>(ModalContext);

  const closeModalHandler = ():void => {
    ModalCtx.toggleModalMenu()
  }
  
  return (
    <Modal>
      <div className="modal__close" onClick={closeModalHandler}>&times;</div>
      <div className="modal-header">
        <h2 className="modal-header__title">Add appointment</h2>
      </div>
      <div className="modal-body">
        <FormMeeting />
      </div>
    </Modal>
  );
};

export default MeetingModal;
