import React, { useState } from "react";
import { useContext } from "react";
import { Button, ButtonOutline } from "../UI/Button";
import { Select } from "../UI/Select";
import { ModalContextObj, MeetingContextObj } from "../../models/ContextObject.class";
import Meeting from '../../models/Meeting.class'
import ModalContext from "../../store/modal-context";
import MeetingContext from "../../store/meeting-context";
import FilterOption from "../../models/FilterOption.class";
import "./FormMeeting.css";


const FormMeeting: React.FC = () => {

  const [enteredTitle, setEnteredTitle] = useState<string>("Code Review");
  const [enteredDate, setEnteredDate] = useState<string>("");
  const [enteredUser, setEnteredUser] = useState<string>("Sandro Müller");
  const [enteredPriority, setEnteredPriority] = useState<string>("High");

  const ModalCtx = useContext<ModalContextObj>(ModalContext);
  const MeetingCtx = useContext<MeetingContextObj>(MeetingContext);

  const titleChangeHandler = (event: React.ChangeEvent<HTMLSelectElement>):void => {
    setEnteredTitle(event.target.value);
  };

  const dateChangeHandler = (event: React.ChangeEvent<HTMLInputElement>):void => {
    setEnteredDate(event.target.value);
  };

  const userChangeHandler = (event: React.ChangeEvent<HTMLSelectElement>):void => {
    setEnteredUser(event.target.value);
  };

  const priorityChangeHandler = (event: React.ChangeEvent<HTMLSelectElement>):void => {
    setEnteredPriority(event.target.value);
  };

  const addMeetingHandler = ():void => {
    const meeting: Meeting = new Meeting(
      enteredTitle,
      new Date(enteredDate),
      enteredUser,
      enteredPriority
    )

    MeetingCtx.addMeeting(meeting);
    ModalCtx.toggleModalMenu();

    // RESET
    setEnteredTitle("");
    setEnteredDate("");
    setEnteredUser("");
    setEnteredPriority("");
  };

  const closeModalHandler = ():void => {
    ModalCtx.toggleModalMenu();
  };

  const filterOptionTitle: FilterOption[] = [
    new FilterOption("Code Review"),
    new FilterOption("Refactor"),
    new FilterOption("Bug Fix"),
  ];

  const filterOptionUser: FilterOption[] = [
    new FilterOption("Sandro Müller"),
    new FilterOption("Anton Borvanov"),
    new FilterOption("Vinay Bedre"),
  ];

  const filterOptionPriority: FilterOption[] = [
    new FilterOption("High"),
    new FilterOption("Medium"),
    new FilterOption("Low"),
  ];

  return (
    <div className="new-meeting-controls">
      <div className="new-meeting-control">
        <label className="new-meeting__label">Title</label>
        <Select onChange={titleChangeHandler}>
          {filterOptionTitle.map((item, index) => (
            <option value={item.optionText} key={index}>
              {item.optionText}
            </option>
          ))}
        </Select>
      </div>
      <div className="new-meeting-control">
        <label className="new-meeting__label">Date</label>
        <input
          type="date"
          className="new-meeting__input"
          value={enteredDate}
          min="2018-01-01"
          max="2022-12-31"
          onChange={dateChangeHandler}
        />
      </div>
      <div className="new-meeting-control">
        <label className="new-meeting__label">User</label>
        <Select onChange={userChangeHandler}>
          {filterOptionUser.map((item, index) => (
            <option value={item.optionText} key={index}>
              {item.optionText}
            </option>
          ))}
        </Select>
      </div>
      <div className="new-meeting-control">
        <label className="new-meeting__label">Priority</label>
        <Select onChange={priorityChangeHandler}>
          {filterOptionPriority.map((item, index) => (
            <option value={item.optionText} key={index}>
              {item.optionText}
            </option>
          ))}
        </Select>
      </div>
      <div className="new-meeting-buttons">
        <ButtonOutline onClick={closeModalHandler}>Close Modal</ButtonOutline>
        <Button onClick={addMeetingHandler}>Add Meeting</Button>
      </div>
    </div>
  );
};

export default FormMeeting;
