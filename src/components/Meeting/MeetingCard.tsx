import React, { useContext } from "react";
import { MeetingContextObj } from "../../models/ContextObject.class";
import Meeting from "../../models/Meeting.class";
import MeetingContext from "../../store/meeting-context"
import "./MeetingCard.css";

const MeetingCard: React.FC<Meeting> = (props) => {
  const day: string = props.date.toLocaleString("en-US", { day: "2-digit" });
  const month: string = props.date.toLocaleString("en-US", { month: "long" });
  const year: number = props.date.getFullYear();

  const MeetingCtx = useContext<MeetingContextObj>(MeetingContext);

  const removeMeetingHandler = ():void => {
    const meetingId:string = props.id
    MeetingCtx.removeMeeting(meetingId)
  }

  return (
    <div className="meeting-card">
      <div className="meeting-card__close" onClick={removeMeetingHandler}>&times;</div>
      <div className="meeting-card-date">
        <span>{day}.</span>
        <span>{month}</span>
        <span>{year}</span>
      </div>
      <div className="meeting-card-info">
        <strong>{props.title}</strong>
        <span>{props.user}</span>
        <span>{props.priority}</span>
      </div>
    </div>
  );
};

export default MeetingCard;
