import React from "react";
import Meeting from "../../models/Meeting.class";
import MeetingCard from "../Meeting/MeetingCard";

const MeetingList: React.FC<{ meetings: Meeting[] }> = (props) => {

  return (
    <div>
      {props.meetings.map((meeting: Meeting) => (
        <MeetingCard
          key={meeting.id}
          id={meeting.id}
          title={meeting.title}
          date={meeting.date}
          user={meeting.user}
          priority={meeting.priority}
        />
      ))}
    </div>
  );
};
export default MeetingList;
