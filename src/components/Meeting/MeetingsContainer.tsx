import { useContext } from "react";
import { ButtonOutline } from "../UI/Button";
import { ModalContextObj } from "../../models/ContextObject.class";
import MeetingList from "./MeetingList";
import ModalContext from "../../store/modal-context";
import Card from "../UI/Card";
import Meeting from "../../models/Meeting.class";
import "./MeetingsContainer.css";

const MeetingsContainer: React.FC<{ meetings: Meeting[]}> = (props) => {
  const totalMeetings: number = props.meetings.length;
  const ModalCtx = useContext<ModalContextObj>(ModalContext);
  let content: any | HTMLParagraphElement; // Should be ReactComponentElement<HTMLElement>
  
  const openMeetingModal = ():void => {
    ModalCtx.toggleModalMenu();
  };

  if (totalMeetings) {
    content = <MeetingList meetings={props.meetings} />;
  } 
  
  if (!totalMeetings){
    content = <p className="meetings-empty">No meetings available.</p>
  }

  return (
    <Card>
      <div className="meetings-header">
        <h1>Appointments</h1>
        <ButtonOutline onClick={openMeetingModal}>Add meeting</ButtonOutline>
      </div>
      {content}
    </Card>
  );
};

export default MeetingsContainer;
