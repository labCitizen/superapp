import "./SideNavigation.css";
import { Select } from "../UI/Select";
import { FilterContextObj } from "../../models/ContextObject.class";
import React, { useContext } from "react";
import FilterOption from "../../models/FilterOption.class";
import FilterContext from "../../store/filter-context";

const SideNavigation: React.FC<{selectedYear: string, onChangeYear: (option: string) => void}> = (props) => {
  const FilterCtx = useContext<FilterContextObj>(FilterContext);
  const filterIsOpen = FilterCtx.filterIsOpen();

  const filterOptionYear: FilterOption[] = [
    new FilterOption("2021"),
    new FilterOption("2020"),
    new FilterOption("2019"),
    new FilterOption("2018"),
  ];

  const dropdownChangeYearHandler = (event: React.ChangeEvent<HTMLSelectElement>):void => {
    const selectedYear = event.target.value;
    props.onChangeYear(selectedYear);
  };

  return (
    <div
      className={`sideNavigation ${filterIsOpen && "sideNavigation--expanded"}`}
    >
      <h3 className="sideNavigation__headline">Filter settings</h3>
      <div className="sideNavigation-filter">
        <label className="sideNavigation-filter__title">Select year</label>
        <Select value={props.selectedYear} onChange={dropdownChangeYearHandler}>
          {filterOptionYear.map((item) => (
            <option value={item.optionText} key={item.optionText}>
              {item.optionText}
            </option> 
          ))}
        </Select>
      </div>
    </div>
  );
};

export default SideNavigation;
