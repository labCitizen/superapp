import "./MainNavigation.css";
import { Link } from "react-router-dom";
import { Button } from "../UI/Button";
import { useContext } from 'react'
import { FilterContextObj } from "../../models/ContextObject.class";
import NavigationLink from "../../models/NavigationLink.class";
import FilterContext from '../../store/filter-context'

const MainNavigation = () => {
  
  const FilterCtx = useContext<FilterContextObj>(FilterContext)
  const filterIsOpen = FilterCtx.filterIsOpen()

  const navigationLinks: NavigationLink[] = [
    new NavigationLink("Home", "/"),
    new NavigationLink("FAQ", "/faq"),
    new NavigationLink("Info", "/info"),
  ];
  
  return (
    <header className="main-navigation">
      <div className="main-navigation-logo">
        <Link to="/" className="main-navigation__link">
          SuperApp
        </Link>
      </div>
      <nav className="main-navigation-links">
        <ul className="main-navigation-list">
          <li className="main-navigation__item">
            {navigationLinks.map((item, index) => (
              <Link to={item.path} className="main-navigation__link" key={index}>
                {item.linkText}
              </Link>
            ))}
          </li>
        </ul>
      </nav>
      <Button onClick={FilterCtx.toggleFilterMenu}>{ !filterIsOpen ? 'Open Filter' : 'Close Filter'}</Button>
    </header>
  );
};

export default MainNavigation;
