import { Route, Switch } from "react-router-dom";
import { useContext } from "react";
import { MeetingContextObj } from "./models/ContextObject.class";
import Layout from "./components/Layout/Layout";
import MeetingContext from "./store/meeting-context";
import Meeting from "./models/Meeting.class";
import Home from "./views/Home";
import Faq from "./views/Faq";
import Info from "./views/Info";

const App = () => {

  const MeetingCtx = useContext<MeetingContextObj>(MeetingContext);
  const meetings: Meeting[] = MeetingCtx.fetchMeetings()

  return (
    <Layout>
      <Switch>
        <Route path="/" exact>
          <Home meetings={meetings} />
        </Route>
        <Route path="/faq">
          <Faq />
        </Route>
        <Route path="/info">
          <Info />
        </Route>
      </Switch>
    </Layout>
  );
}

export default App;
