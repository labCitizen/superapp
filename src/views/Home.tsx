import { useState } from "react";
import { Backdrop } from "../components/UI/Backdrop";
import { useContext } from "react";
import { FilterContextObj } from "../models/ContextObject.class";
import SideNavigation from "../components/Navigation/SideNavigation";
import MeetingsContainer from "../components/Meeting/MeetingsContainer";
import FilterContext from "../store/filter-context";
import MeetingModal from "../components/Modal/MeetingModal";
import Meeting from "../models/Meeting.class";

const Home: React.FC<{meetings: Meeting[]}> = (props) => {
  const FilterCtx = useContext<FilterContextObj>(FilterContext);
  const filterIsOpen = FilterCtx.filterIsOpen();

  const [filteredYear, setFilteredYear] = useState<string>("2021");
  const filterChangeYearHandler = (selectedYear: string):void => {
    setFilteredYear(selectedYear);
  };

  const filteredMeetings: Meeting[] = props.meetings.filter((meeting: Meeting) => {
    return meeting.date.getFullYear().toString() === filteredYear;
  });

  // QUESTION: How to pass props to Backdrop?
  return (
    <div id="home" className="home">
      <Backdrop style={{ display: filterIsOpen ? "block" : "none" }} />
      <SideNavigation
        onChangeYear={filterChangeYearHandler}
        selectedYear={filteredYear}
      />
      
      <MeetingModal />
      <MeetingsContainer meetings={filteredMeetings} />
    </div>
  );
};

export default Home;
