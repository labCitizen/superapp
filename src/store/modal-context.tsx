//@ts-nocheck

import React, { createContext, useState } from "react";
import { ModalContextObj } from "../models/ContextObject.class";

const ModalContext = createContext<ModalContextObj>({
  isModal: false,
  toggleModalMenu: () => {},
  modalIsOpen: () => {}
});

export const ModalContextProvider: React.FC = (props) => {
  const [isModal, setIsModal] = useState<boolean>(false);

  const toggleModalMenuHandler = ():void => {
    setIsModal((prevState) => {
      return !prevState;
    });
  };

  const modalIsOpenHandler = (): boolean => {
    return isModal;
  };

  const context: ModalContextObj = {
    isModal: isModal,
    toggleModalMenu: toggleModalMenuHandler,
    modalIsOpen: modalIsOpenHandler,
  };

  return (
    <ModalContext.Provider value={context}>
      {props.children}
    </ModalContext.Provider>
  );
}

export default ModalContext;
