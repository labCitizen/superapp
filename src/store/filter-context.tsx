//@ts-nocheck
import React, { createContext, useState } from "react";
import { FilterContextObj } from "../models/ContextObject.class";

const FilterContext = createContext<FilterContextObj>({
  isFilter: false,
  toggleFilterMenu: () => {},
  filterIsOpen: () => {},
});

export const FilterContextProvider: React.FC = (props) => {
  const [isFilter, setIsFilter] = useState<boolean>(false);

  const toggleFilterMenuHandler = (): void => {
    setIsFilter((prevState) => {
      return !prevState;
    });
  };

  const filterIsOpenHandler = (): boolean => {
    return isFilter;
  };

  const context: FilterContextObj = {
    isFilter: isFilter,
    toggleFilterMenu: toggleFilterMenuHandler,
    filterIsOpen: filterIsOpenHandler,
  };

  return (
    <FilterContext.Provider value={context}>
      {props.children}
    </FilterContext.Provider>
  );
};

export default FilterContext;
