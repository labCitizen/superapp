//@ts-nocheck
import React, { createContext, useState } from "react";
import { MeetingContextObj } from "../models/ContextObject.class";
import Meeting from "../models/Meeting.class";


const DUMMY_MEETINGS: Meeting[] = [
  {
    id: "m1",
    title: "Code Review",
    date: new Date(2020, 7, 14),
    user: "Sandro Müller",
    priority: "High",
  },
  {
    id: "m2",
    title: "Code Review",
    date: new Date(2020, 7, 14),
    user: "Sandro Müller",
    priority: "High",
  },
  {
    id: "m3",
    title: "Code Review",
    date: new Date(2020, 7, 14),
    user: "Sandro Müller",
    priority: "High",
  },
  {
    id: "m4",
    title: "Code Review",
    date: new Date(2020, 7, 14),
    user: "Sandro Müller",
    priority: "High",
  },
  {
    id: "m5",
    title: "Code Review",
    date: new Date(2021, 7, 14),
    user: "Sandro Müller",
    priority: "High",
  },
  {
    id: "m6",
    title: "Code Review",
    date: new Date(2021, 7, 14),
    user: "Sandro Müller",
    priority: "High",
  },
  {
    id: "m7",
    title: "Code Review",
    date: new Date(2022, 7, 14),
    user: "Sandro Müller",
    priority: "High",
  },
  {
    id: "m8",
    title: "Code Review",
    date: new Date(2022, 7, 14),
    user: "Sandro Müller",
    priority: "High",
  },
  {
    id: "m9",
    title: "Code Review",
    date: new Date(2022, 7, 14),
    user: "Sandro Müller",
    priority: "High",
  },
  {
    id: "m10",
    title: "Code Review",
    date: new Date(2019, 7, 14),
    user: "Sandro Müller",
    priority: "High",
  },
];

const MeetingContext = createContext<MeetingContextObj>({
  meetings: [],
  totalMeetings: 0,
  fetchMeetings: () => {},
  addMeeting: (newMeeting: Meeting) => {},
  removeMeeting: (meetingId: string) => {},
  itemIsMeeting: (meetingId: string) => {},
});
 
export const MeetingContextProvider: React.FC = (props) => {
  const [userMeetings, setUserMeetings] = useState<Meeting[]>(DUMMY_MEETINGS);

  const fetchMeetingsHandler = (): Meeting[] => {
    return userMeetings;
  };

  const addMeetingHandler = (newMeeting: Meeting): void => {
    setUserMeetings((prevUserMeetings) => {
      return prevUserMeetings.concat(newMeeting);
    });
  };

  const removeMeetingHandler = (meetingId: string): void => {
    setUserMeetings((prevUserMeetings) => {
      return prevUserMeetings.filter((meeting) => meeting.id !== meetingId);
    });
  };

  const itemIsMeetingHandler = (meetingId: string): boolean => {
    return userMeetings.some((meeting) => meeting.id === meetingId);
  };

  const context: MeetingContextObj = {
    meetings: userMeetings,
    totalMeetings: userMeetings.length,
    fetchMeetings: fetchMeetingsHandler,
    addMeeting: addMeetingHandler,
    removeMeeting: removeMeetingHandler,
    itemIsMeeting: itemIsMeetingHandler
  };

  return (
    <MeetingContext.Provider value={context}>
      {props.children}
    </MeetingContext.Provider>
  );
}

export default MeetingContext;
