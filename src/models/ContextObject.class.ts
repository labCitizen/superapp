import Meeting from './Meeting.class'

export type ModalContextObj = {
    isModal: boolean;
    toggleModalMenu: () => boolean,
    modalIsOpen: () => boolean
}

export type MeetingContextObj = {
    meetings: Meeting[];
    totalMeetings: number;
    fetchMeetings: () => Meeting[];
    addMeeting: (newMeeting: Meeting) => void;
    removeMeeting: (meetingId: string) => void;
    itemIsMeeting: (meetingId: string) => boolean;
}

export type FilterContextObj = {
    isFilter: boolean;
    toggleFilterMenu: () => void,
    filterIsOpen: () => boolean
}