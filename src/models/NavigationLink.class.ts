
class NavigationLink {
    linkText: string;
    path: string;

    constructor(linkText: string, path: string) {
        this.linkText = linkText;
        this.path = path;
    }
}

export default NavigationLink