import uuid from 'react-native-uuid';

class Meeting {
    id: string;
    title: string;
    date: Date;
    user: string;
    priority: string;

    constructor(title: string, date: Date, user: string, priority: string) {
        this.id = uuid.v4().toString()
        this.title = title;
        this.date = date;
        this.user = user;
        this.priority = priority;
    }
}

export default Meeting;