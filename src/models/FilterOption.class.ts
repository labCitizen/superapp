
class FilterOption {
    optionText: string;

    constructor(optionText: string) {
        this.optionText = optionText;
    }
}

export default FilterOption;